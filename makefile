TITLE="v2d.fnl"
VERSION="0.1.0"
AUTHOR="AlexJGriffith"
DESCRIPTION="An optionally destructive 2d vector math library"
FENNEL=fennel
ARGS=--no-metadata

## Replace inline text in code
## There is probably a better way to do this
DESTRUCTIVE="s/(local d0x02 false)/(local d0x02 true)/p"
TEST="s/(local t0x01 false)/(local t0x01 true)/p"
DHEAD="s/:__DESTRUCTIVE false ;;0x03/:__DESTRUCTIVE true ;;0x03/p"
THEAD="s/:__TESTS false ;;0x04/:__TESTS true ;;0x04/p"

build:
	mkdir -p release
	$(FENNEL) --compile $(ARGS) v2d.fnl > release/v2d.lua

destructive:
	mkdir -p release
	sed ${DESTRUCTIVE} v2d.fnl | \
	sed $(DHEAD) > release/tmp.fnl
	$(FENNEL) --compile release/tmp.fnl > release/v2d.lua
	rm release/tmp.fnl

test:
	mkdir -p release
	sed ${TEST} v2d.fnl | sed ${THEAD} > release/tmp.fnl
	$(FENNEL) release/tmp.fnl
	rm release/tmp.fnl

destructive-test:
	mkdir -p release
	sed ${DESTRUCTIVE} v2d.fnl | \
	sed ${TEST}   | \
	sed ${DHEAD}  | \
	sed ${THEAD}  > release/tmp.fnl
	$(FENNEL) release/tmp.fnl
	rm release/tmp.fnl

clean:
	rm -rf release/*.fnl release/*.lua
