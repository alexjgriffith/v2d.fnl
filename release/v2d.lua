local v2d = {__TITLE = "v2d", __DESCRIPTION = "An optionally destructive 2d vector math library", __AUTHOR = "AlexJGriffith", __VERSION = "0.1.0", __LICENCE = "GPL3+", __DESTRUCTIVE = false, __TESTS = false}
local min_error = 1e-14
local sqrt = math.sqrt
local atan2 = math.atan2
local sin = math.sin
local cos = math.cos
local abs = math.abs
local random = math.random
v2d.identity = {1, 1}
v2d.zeros = {0, 0}
v2d.eq = function(va, vb)
  return ((va[1] == vb[1]) and (va[2] == vb[2]))
end
v2d.neq = function(va, vb)
  return ((va[1] ~= vb[1]) or (va[2] ~= vb[2]))
end
v2d.mult = function(va, scaler)
  return {(scaler * va[1]), (scaler * va[2])}
end
v2d.incr = function(va, scaler)
  return {(scaler + va[1]), (scaler + va[2])}
end
v2d["or"] = function(va, vb)
  if (va and ("table" == type(va))) then
    return va
  else
    return vb
  end
end
v2d.mag = function(va)
  return sqrt(((va[1] ^ 2) + (va[2] ^ 2)))
end
v2d.mag2 = function(va)
  return ((va[1] ^ 2) + (va[2] ^ 2))
end
v2d.dot = function(va, vb)
  return ((va[1] * vb[1]) + (va[2] * vb[2]))
end
v2d.normalize = function(va)
  local mag = v2d.mag(va)
  return {(va[1] / mag), (va[2] / mag)}
end
v2d.add = function(va, vb)
  return {(va[1] + vb[1]), (va[2] + vb[2])}
end
v2d.sub = function(va, vb)
  return {(va[1] - vb[1]), (va[2] - vb[2])}
end
v2d.det = function(va, vb)
  local a = va[1]
  local b = va[2]
  local c = vb[1]
  local d = vb[2]
  return ((a * d) - (b * c))
end
v2d.norm = function(va, vb)
  local dx = (va[1] - vb[1])
  local dy = (va[2] - vb[2])
  return {( - dy), dx}, {dy, ( - dx)}
end
v2d.unorm = function(va, vb)
  local dx = (va[1] - vb[1])
  local dy = (va[2] - vb[2])
  return v2d.normalize({( - dy), dx}), v2d.normalize({dy, ( - dx)})
end
v2d.euclid = function(va, vb)
  return sqrt((((va[1] - vb[1]) ^ 2) + ((va[2] - vb[2]) ^ 2)))
end
v2d.euclid2 = function(va, vb)
  return (((va[1] - vb[1]) ^ 2) + ((va[2] - vb[2]) ^ 2))
end
v2d["min-euclid2"] = function(min)
  local function _2_(va, vb)
    return (v2d.euclid(va, vb) < min)
  end
  return _2_
end
v2d.manhatten = function(va, vb)
  return (abs((va[1] - vb[1])) + abs((va[2] - vb[2])))
end
v2d.random = function(x1, y1, x2, y2)
  if (x1 and y1 and x2 and y2) then
    return {random(x1, x2), random(y1, y2)}
  elseif (x1 and y1) then
    return {random(x1), random(y1)}
  elseif x1 then
    return {random(x1), random(x1)}
  else
    return {random(), random()}
  end
end
v2d.limit = function(va, sc)
  local mag = v2d.mag(va)
  local ratio = (sc / mag)
  return v2d.mult(va, ratio)
end
v2d["vec-to-ang"] = function(va)
  return atan2(va[2], va[1])
end
v2d["ang-to-vec"] = function(theta)
  return {cos(theta), sin(theta)}
end
v2d["ang-rotate"] = function(va, theta)
  local si = sin(theta)
  local co = cos(theta)
  local x = va[1]
  local y = va[2]
  return {((co * x) + (si * y)), ((co * y) - (si * x))}
end
v2d["vec-rotate"] = function(va, vb)
  return v2d["ang-rotate"](va, v2d["vec-to-ang"](vb))
end
return v2d
