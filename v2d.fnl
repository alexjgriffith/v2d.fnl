(local v2d {:__TITLE "v2d"
            :__DESCRIPTION "An optionally destructive 2d vector math library"
            :__AUTHOR "AlexJGriffith"
            :__VERSION "0.1.0"
            :__LICENCE "GPL3+"
            :__DESTRUCTIVE false ;;0x03
            :__TESTS false ;;0x04
            })

(local min-error 0.00000000000001)
(macro test [name fun comp result]
  (local t0x01 false)
  (when t0x01 ;; Set to false when distributing
    `(do
       (local fennel# (require :fennel))
       (print (fennel#.view
               [,name (if (,comp ,fun ,result)
                          :pass
                          (values :fail ,fun ,result))])))))

;; Use this when returning a vector from a function
;; toggle to true if you wish the compiled functions
;; to be destructive
(macro vec! [v a b]
  "toggle destructive to overwrite the first vector argument
in most calls."
  (local d0x02 false)
  (if d0x02 
      `(do (tset ,v 1 ,a)
           (tset ,v 2 ,b)
           ,v)
      `[,a ,b]))

(local sqrt math.sqrt)
(local atan2 math.atan2)
(local sin math.sin)
(local cos math.cos)
(local abs math.abs)
(local random math.random)

(macro f [v]
  `(. ,v 1))

(macro s [v]
  `(. ,v 2))

(set v2d.identity [1 1])

(set v2d.zeros [0 0])

(fn v2d.eq [va vb]
  "Compare two vectors va == vb. (Vec, Vec) => bool"
  (and (= (f va) (f vb))
       (= (s va) (s vb))))
(test "v2d.eq-1" (v2d.eq [1 1] [1 1]) = true)
(test "v2d.eq-2" (v2d.eq [1 1] [1 2]) ~= true)

(fn v2d.neq [va vb]
  "Compare two vectors va != vb. (Vec, Vec) => bool"
  (or (~= (f va) (f vb))
       (~= (s va) (s vb))))
(test "v2d.neq-1" (v2d.neq [1 1] [1 1]) = false)
(test "v2d.neq-2" (v2d.neq [1 1] [1 2]) ~= false)

(fn v2d.mult [va scaler]
  "Multiply vector va by scaler. (Vec, Sc) => Vec"
  (vec! va (* scaler (f va)) (* scaler (s va))))
(test "v2d.mult-1" (v2d.mult [1 1] 2) v2d.neq [1 2])
(test "v2d.mult-2" (v2d.mult [1 1] 2) v2d.eq [2 2])

(fn v2d.incr [va scaler]
  "Increment vector va by scaler. (Vec, Sc) => Vec"
  (vec! va (+ scaler (f va)) (+ scaler (s va))))
(test "v2d.incr-1" (v2d.incr [1 1] 2) v2d.eq [3 3])
(test "v2d.incr-2" (v2d.incr [1 1] 2) v2d.neq [3 2])

(fn v2d.or [va vb]
  "If va is nil return vb. (Vec,Vec) => Vec"
  (if (and va (= "table" (type va))) va vb))
(test "v2d.or-1" (v2d.or nil [1 1]) v2d.eq [1 1])
(test "v2d.or-2" (v2d.or [1 1] [2 2]) v2d.eq [1 1])

(fn v2d.mag [va]
  "Determine magnatude of va . (Vec) => Sc"
  ;; sqrt faster than finsq method in luajit  
  (sqrt (+ (^ (f va) 2)
           (^ (s va) 2))))
(test "v2d.mag-1" (v2d.mag [1 1]) = (sqrt 2))
(test "v2d.mag-2" (v2d.mag [4 4]) = (sqrt 32))

(fn v2d.mag2 [va]
  "Determine magnatude of va . (Vec) => Sc"
  ;; sqrt faster than finsq method in luajit  
  (+ (^ (f va) 2)
     (^ (s va) 2)))
(test "v2d.mag2-1" (v2d.mag2 [1 1]) = 2)
(test "v2d.mag2-2" (v2d.mag2 [4 4]) =  32)

(fn v2d.dot [va vb]
  "Detrmine the dot product of va.vb. (Vec,Vec) => Sc"
  (+ (* (f va) (f vb)) (* (s va) (s vb))))
(test "v2d.dot" (v2d.dot [1 1] [2 2]) = 4)

(fn v2d.normalize [va]
  "Normalize Vector va. (Vec) => Vec"
  (let [mag (v2d.mag va)]
    (vec! va (/ (f va) mag) (/ (s va) mag))))
(test "v2d.normalize" (v2d.normalize [1 1]) v2d.eq
      [(/ 1 (sqrt 2)) (/ 1 (sqrt 2))])

(fn v2d.add [va vb]
  "Add Vector va to Vecor vb. (Vec,Vec) => Vec"
  (vec! va (+ (f va) (f vb)) (+ (s va) (s vb))))
(test "v2d.add-1" (v2d.add [1 1] [2 2]) v2d.eq [3 3])
(test "v2d.add-2" (v2d.add [1 1] [2 2]) v2d.neq [-2 -2])

(fn v2d.sub [va vb]
  "Subtract Vector va from Vecor vb. (Vec,Vec) => Vec"
  (vec! va (- (f va) (f vb)) (- (s va) (s vb))))
(test "v2d.sub-1" (v2d.sub [1 1] [2 1]) v2d.eq [-1 0])
(test "v2d.sub-2" (v2d.sub [1 1] [0 1]) v2d.eq [1 0])

(fn v2d.det [va vb]
  "Determinant of Vector va and Vector vb. (Vec,Vec) => Sc"
  (let [a (f va)
        b (s va)
        c (f vb)
        d (s vb)]
    (- (* a d) (* b c))))
(test "v2d.det-1" (v2d.det [1 1] [1 1]) = 0)
(test "v2d.det-2" (v2d.det [1 1] [0 1]) = 1)
(test "v2d.det-3" (v2d.det [1 1] [1 0]) = -1)

(fn v2d.norm [va vb]
  "Find the normal of Vectors va and vb. Never Destructive. (Vec, Vec) => Vec"
  (let [dx (- (f va) (f vb))
        dy (- (s va) (s vb))]
    (values [(- dy) dx] [dy (- dx)])))
(test "v2d.norm-1" (v2d.norm [1 1] [1 1]) v2d.eq [0 0])
(test "v2d.norm-2" (v2d.norm [1 1] [1 0]) v2d.eq [-1 0])

(fn v2d.unorm [va vb]
  "Find the normal of Vectors va and vb. Never Destructive. (Vec, Vec) => Vec"
  (let [dx (- (f va) (f vb))
        dy (- (s va) (s vb))]
    (values (v2d.normalize [(- dy) dx])
            (v2d.normalize [dy (- dx)]))))
(test "v2d.unorm" (v2d.norm [1 1] [1 0]) v2d.eq [-1 0])

(fn v2d.euclid [va vb]
  "Euclid distance (mag) of Vectors vb and va. (Vec, Vec) => Sc"
  (sqrt (+ (^ (- (f va) (f vb)) 2)
           (^ (- (s va) (s vb)) 2))))
(test "v2d.euclid" (v2d.euclid [0 0] [1 1]) = (sqrt 2))

(fn v2d.euclid2 [va vb]
  "Square euclid distance (mag) of Vectors vb and va. (Vec, Vec) => Sc"
  (+ (^ (- (f va) (f vb)) 2)
     (^ (- (s va) (s vb)) 2)))
(test "v2d.euclid2" (v2d.euclid2 [0 0] [1 1]) = 2)

(fn v2d.min-euclid2 [min]
  (fn [va vb]
    (< (v2d.euclid va vb) min)))

(fn v2d.manhattan [va vb]
  "Mannhatten distance (mag) of Vectors vb and va. (Vec, Vec) => Sc"
  (+ (abs (- (f va) (f vb)))
     (abs (- (s va) (s vb)))))
(test "v2d.manhattan" (v2d.manhattan [0 0] [1 1]) = 2)

(fn v2d.random [x1 y1 x2 y2]
  "Random vec between x1 x2 and y1 y2. (Sc,Sc,Sc,Sc) => Vec"
  (if (and x1 y1 x2 y2) [(random x1 x2) (random y1 y2)]
      (and x1 y1) [(random x1) (random y1)]
      x1 [(random x1) (random x1)]
      [(random) (random)]))
(test "v2d.random-1" (f (v2d.random)) < 1 )
(test "v2d.random-2" (s (v2d.random)) < 1 )
(test "v2d.random-3" (f (v2d.random 2 2 4 4)) > 1 )
(test "v2d.random-4" (s (v2d.random 2 2 4 4)) > 1 )
      
(fn v2d.limit [va sc]
  "Limit the magnatude of va to sc. (Vec,Sc) => Vec"
  (let [mag (v2d.mag va)
        ratio (/ sc mag)]
    (v2d.mult va ratio)))
(test "v2d.limit-1" (v2d.limit [1 1] 1) v2d.eq [(/ 1 (sqrt 2)) (/ 1 (sqrt 2))])
(test "v2d.limit-2" (v2d.limit [1 1] 2) v2d.eq [(/ 2 (sqrt 2)) (/ 2 (sqrt 2))])

(fn v2d.vec-to-ang [va]
  "Convert a vector to an angle (Vec) => Ang"
  (atan2 (s va) (f va)))
(test "v2d.vec-to-ang-1" (v2d.vec-to-ang [0 1]) = (/ math.pi 2))
(test "v2d.vec-to-ang-2" (v2d.vec-to-ang [1 0]) = 0)

(fn v2d.ang-to-vec [theta]
  "Convert an angle to vector (Ang) => Vec"
  [(cos theta) (sin theta)])
(test "v2d.ang-to-vec-1" (v2d.ang-to-vec (/ math.pi 4))
      (v2d.min-euclid2 min-error) [(/ 1 (sqrt 2)) (/ 1 (sqrt 2))])
(test "v2d.ang-to-vec-2" (v2d.ang-to-vec 0) v2d.eq [1 0])

(fn v2d.ang-rotate [va theta]
  (let [si (sin theta)
        co (cos theta)
        x (f va)
        y (s va)]
  (vec! va (+ (* co x) (* si y)) (- (* co y) (* si x)))))

(fn v2d.vec-rotate [va vb]
  (v2d.ang-rotate va (v2d.vec-to-ang vb)))
(test "v2d.vec-rotate-1" (v2d.vec-rotate [1 1] [0 1]) (v2d.min-euclid2 min-error) [1 -1])

v2d
